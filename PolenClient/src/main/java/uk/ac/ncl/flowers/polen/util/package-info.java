/**
 * Utility classes for the POLEN messaging system
 * 
 * @author Goksel Misirli
 */
package uk.ac.ncl.flowers.polen.util;